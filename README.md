# OPS: Off-the-grid Package made Simple 🎻

Please note that this version is NOT retrocompatible with the papers published before June 2022. Visit [the former git repo](https://github.com/XeBasTeX/SFW-python) to review the old scripts.


## TODO

See below the progress of the whole project (yes, it's a quite long journey! 🐢):


| Section    | Description                                                                      		|    Status |
|------------|------------------------------------------------------------------------------------------|----------:|
| Dirac      | Reconstruct points from one image                                                		|    Done 🟢 |
| Covariance | Reconstruct static points with fluctuating <br>luminosity from a temporalstack of images |    Done 🟢 |
| Curves     | Reconstruct points from one image                                                		| Ongoing 🚧 |
| Dynamic    | Reconstruct dynamic points in a <br>blind  inverse problem settings              		|    Future 🔴 |


Also note that the following functionalities are not implemented at this point:
* rectangular domain (only square at this point);
* domain with periodic boundaries (`\mathbb{T}_2`);
* Fourier (partial support), Laplace (no support whatsoever);
* a web documentation, such as readthedocs.io



<!-- ## Contents


## Installation and playground


## Documentation -->


# Contribute ![(git logo)](https://yousername.gitlab.io/img/logo_git_50x50.png) 

Maintainers: Bastien Laville (bastien.laville@inria.fr).

Contributors: Bastien Laville.

Distribution and License: Inria, [CeCILL](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) 

Upstream git repository: https://gitlab.inria.fr/blaville/off-the-grid-python

Previous upstream git repository (2021-2022): https://github.com/XeBasTeX/SFW-python
