#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='offgrid',
      version='1.0',
      description='OPS: Off-the-grid Package made simple',
      long_description=open('README.md').read(),
      url='https://gitlab.inria.fr/blaville',
      author='Bastien Laville',
      author_email='bastien.laville@pm.me, bastien.laville@inria.fr',
      license='CeCILL',
      packages=['offgrid'],
      zip_safe=False,
      install_requires=[
          "numpy",
          "pytorch",
          ],)